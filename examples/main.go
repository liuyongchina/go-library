package main

import (
	"flag"
	"fmt"
	"time"

	"gitee.com/liuyongchina/go-library/config"
	"gitee.com/liuyongchina/go-library/libs/ycommon"
	"gitee.com/liuyongchina/go-library/libs/yelastic"
	"gitee.com/liuyongchina/go-library/libs/yqiniu"
)

func main() {
	config.TomlPath = "/Users/liuyong/go/src/go-library/toml/toml_dev/"
	//testES()
	//esLocation()
	//getQiniuToken()
	//testFlag()
	//testCamp()
	testTime()
}
func testTime() {
	now := time.Now()
	//newTIme, _ := ycommon.GetCalculateTime(now, "15h")
	//newTime := now.Add(60 * time.Second)
	fmt.Println(now.Format(time.RubyDate))
}
func testCamp() {
	str := "test_lower_data"
	newstr := ycommon.UnderscoreToUpperCamelCase(str)
	fmt.Println(newstr)
	tstr := ycommon.CamelCaseToUnderscore(newstr)
	fmt.Println(tstr)
}
func testFlag() {
	//go run examples/main.go -word=11 -numb=2222
	wordPtr := flag.String("word", "foo", "a string")
	numbPtr := flag.Int("numb", 42, "an int")
	flag.Parse()
	fmt.Println("word", *wordPtr)
	fmt.Println("numbPtr", *numbPtr)
}
func getQiniuToken() {
	token := yqiniu.GetSimpleToken("user")
	fmt.Println(token)
}
func testES() {
	// var textData = make(map[string]interface{})

	// textData["title"] = "now go"
	// textData["content"] = "liuyong123 test"
	// body, err := yelastic.NewES().CreateDocument("blog", "1", textData)
	// textData["title"] = "test go2"
	// textData["content"] = "liuyong1234"
	// body, err = yelastic.NewES().CreateDocument("blog", "2", textData)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println("index:", gjson.GetBytes(body, "_index").String())

	// //body, err = yelastic.NewES().WordSearch("title", "test", "blog")
	// body, _ = yelastic.NewES().WordMultiSearch("test", []string{"title", "content"}, "blog")
	// //var hits = gjson.GetBytes(body, "hits.hits").Array()
	// //fmt.Println(hits[0])
	// gjson.GetBytes(body, "hits.hits").ForEach(func(key, value gjson.Result) bool {
	// 	fmt.Println(value.Map()["_source"].Map())
	// 	return true

	// })

}
func esLocation() {
	// yelastic.NewES().GetMapping("cities")
	// return
	//	var textData = make(map[string]interface{})
	// mapData := map[string]interface{}{
	// 	"properties": map[string]interface{}{
	// 		"location": map[string]interface{}{
	// 			"type":  "geo_point",
	// 			"index": "true",
	// 		},
	// 		"name": map[string]interface{}{
	// 			"type": "keyword",
	// 		},
	// 	},
	// }
	// yelastic.NewES().CreateIndex("localcities")
	// yelastic.NewES().PutMapping(mapData, []string{"localcities/doc"})
	// return
	// textData["name"] = "北京"
	// textData["location"] = "39.9047253699,116.4072154982"
	// yelastic.NewES().CreateDocument("city", "1", textData)
	// textData["name"] = "天津"
	// textData["location"] = "39.0850853357,117.1993482089"
	// yelastic.NewES().CreateDocument("city", "2", textData)
	// textData["name"] = "上海"
	// textData["location"] = "31.2304324029,121.4737919321"
	// yelastic.NewES().CreateDocument("city", "3", textData)
	body, _ := yelastic.NewES().SearchByLocation("1000", "39.9047253699", "116.4072154982", "cities", "doc", 1, 20)
	fmt.Println(body)
}
