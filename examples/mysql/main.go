package main

import (
	"fmt"

	"gitee.com/liuyongchina/go-library/config"
	"gitee.com/liuyongchina/go-library/libs/ymysql"
	"gitee.com/liuyongchina/go-library/libs/ymysql/generate"
	"gitee.com/liuyongchina/go-library/libs/yredis"
	"gorm.io/gorm"
)

func main() {
	config.TomlPath = "/Users/liuyong/go/src/go-library/toml/toml_dev/"
	// config.InitConfig()
	// fmt.Println(config.DB.Schema["blog"])

	fmt.Println(ymysql.DB("test"))
	conf := config.NewDB().GetConfig()
	fmt.Println(conf)
	//fmt.Println(ymysql.SSHDB("test"))
	gene(ymysql.DB("test"))
	//testRedis()
}
func testRedis() {
	rds := yredis.Redis("test")
	_ = rds.Set("test", "111", 0)
	cmdValue := rds.Get("test")
	fmt.Println(cmdValue.Val())
}
func gene(db *gorm.DB) {
	modePath := "/Users/liuyong/go/src/go-library/examples/mysql/models/"
	generate.NewGenerator(db, modePath, "test").Genertate("roles")
}
