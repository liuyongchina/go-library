package models

import "time"

type Roles struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	DisplayName string    `json:"display_name"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

func NewRoles() *Roles {
	return &Roles{}
}
func (*Roles) TableName() string {
	return "roles"
}
