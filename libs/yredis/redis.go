package yredis

import (
	"gitee.com/liuyongchina/go-library/config"
	"github.com/go-redis/redis"
)

var rdbs = make(map[string]*redis.Client)

func Redis(rdb string) *redis.Client {
	if rdbs[rdb] != nil {
		return rdbs[rdb]
	}
	redisConfig := config.NewRedis().GetConfig().Rds[rdb]
	rdbs[rdb] = redis.NewClient(&redis.Options{
		Addr:     redisConfig.Addr,
		Password: redisConfig.Password, // no password set
		DB:       redisConfig.DB,       // use default DB
	})
	return rdbs[rdb]
}
