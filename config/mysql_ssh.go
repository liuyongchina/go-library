package config

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

type SDatabase struct {
	Schema map[string]sdb `toml:"mysql_ssh"`
}
type sdb struct {
	Host     string
	Username string
	Password string
	Schema   string
	Ssh      string
	SshUser  string `toml:"ssh_user"`
	SshID    string `toml:"ssh_id"`
}

func (dma *SDatabase) getTomlFile() string {
	return "mysql_ssh.toml"
}

func NewSDB() *SDatabase {
	return &SDatabase{}
}

//GetConfig dbConf := conf.NewDatabase().GetConfig()
func (dma *SDatabase) GetConfig() *SDatabase {
	if _, err := toml.DecodeFile(TomlPath+dma.getTomlFile(), &dma); err != nil {
		fmt.Println(err)
		return dma
	}
	return dma
}
