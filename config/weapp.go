package config

import (
	"fmt"
	"sync"

	"github.com/BurntSushi/toml"
)

var (
	weappConfig *Weapp
	weappOnce   sync.Once
)

type Weapp struct {
	Account map[string]miniapp `toml:"weapp"`
}
type miniapp struct {
	Appid  string
	Secret string
}

func (dma *Weapp) getTomlFile() string {
	return "weapp.toml"
}

func NewWeapp() *Weapp {
	return &Weapp{}
}

//GetConfig dbConf := conf.NewDatabase().GetConfig()
func (dma *Weapp) GetConfig() *Weapp {
	weappOnce.Do(func() {
		if _, err := toml.DecodeFile(TomlPath+dma.getTomlFile(), &dma); err != nil {
			fmt.Println(err)
		}
		weappConfig = dma
	})
	return weappConfig
}
