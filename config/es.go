package config

import (
	"fmt"
	"sync"

	"github.com/BurntSushi/toml"
)

var (
	esConfig *Es
	esOnce   sync.Once
)

type Es struct {
	Addresses []string
	Username  string
	Password  string
	Index     map[string]Index `toml:"index"`
}
type Index struct {
	Index string
	Doc   string
}

func (dma *Es) getTomlFile() string {
	return "es.toml"
}

func NewES() *Es {
	return &Es{}
}

//GetConfig dbConf := conf.NewDatabase().GetConfig()
func (dma *Es) GetConfig() *Es {
	esOnce.Do(func() {
		if _, err := toml.DecodeFile(TomlPath+dma.getTomlFile(), &dma); err != nil {
			fmt.Println(err)
		}
		esConfig = dma
	})

	return esConfig
}
