package config

import (
	"fmt"
	"sync"

	"github.com/BurntSushi/toml"
)

var (
	rdsConfig *Redis
	rdsOnce   sync.Once
)

type Redis struct {
	Rds map[string]rds `toml:"redis"`
}
type rds struct {
	Addr     string
	Password string
	DB       int `toml:"db"`
}

func (dma *Redis) getTomlFile() string {
	return "redis.toml"
}

func NewRedis() *Redis {
	return &Redis{}
}

//GetConfig dbConf := conf.NewDatabase().GetConfig()
func (dma *Redis) GetConfig() *Redis {
	rdsOnce.Do(func() {
		if _, err := toml.DecodeFile(TomlPath+dma.getTomlFile(), &dma); err != nil {
			fmt.Println(err)
		}
		rdsConfig = dma
	})

	return rdsConfig
}
