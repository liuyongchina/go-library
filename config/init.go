package config

import "sync"

var (
	TomlPath       = "godemo/demos/toml/"
	RemoteTomlPath = "http://liuyong.m.soyoung.com/"
	ConfigOnce     sync.Once
)
