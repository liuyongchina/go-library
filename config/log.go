package config

import (
	"fmt"
	"time"

	"github.com/BurntSushi/toml"
)

// Config ...
type YLogConfig struct {
	Development   bool         //开发，线上环境
	InfoPath      string       `toml:"info_path"`
	ErrorPath     string       `toml:"error_path"`
	EncodeConfig  EncodeConfig `toml:"encode_config"`
	RotationLogs  RotateLogs
	Async         bool
	FlushInterval time.Duration `toml:"flush_interval"` //异步刷盘时间间隔
	FlushSize     int           `toml:"flush_size"`     //异步刷盘buffer大小 单位k
}
type RotateLogs struct {
	MaxAge       time.Duration `toml:"max_age"`       // 保存小时数
	RotationTime time.Duration `toml:"rotation_time"` //切割频率 小时记录
}
type EncodeConfig struct {
	MessageKey string `toml:"message_key"`
	LevelKey   string `toml:"level_key"`
	TimeKey    string `toml:"time_key"`
	CallerKey  string `toml:"caller_key"`
}

func (c *YLogConfig) getTomlFile() string {
	return "log.toml"
}

func NewLog() *YLogConfig {
	return &YLogConfig{}
}

//GetConfig dbConf := conf.NewDatabase().GetConfig()
func (c *YLogConfig) GetConfig() *YLogConfig {
	if _, err := toml.DecodeFile(TomlPath+c.getTomlFile(), &c); err != nil {
		fmt.Println(err)
		return c
	}
	c.RotationLogs.MaxAge = c.RotationLogs.MaxAge * time.Hour
	c.FlushInterval = c.FlushInterval * time.Second
	c.FlushSize = c.FlushSize * 1024
	return c
}
