package config

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

var qiniuConfig *Qiniu

type Qiniu struct {
	Ak     string
	Sk     string
	Bucket map[string]Bucket `toml:"bucket"`
}
type Bucket struct {
	BucketName string `toml:"bucket_name"`
}

func (dma *Qiniu) getTomlFile() string {
	return "qiniu.toml"
}

func NewQiniu() *Qiniu {
	return &Qiniu{}
}

//GetConfig dbConf := conf.NewDatabase().GetConfig()
func (dma *Qiniu) GetConfig() *Qiniu {
	ConfigOnce.Do(func() {
		if _, err := toml.DecodeFile(TomlPath+dma.getTomlFile(), &dma); err != nil {
			fmt.Println(err)
		}
		qiniuConfig = dma
	})

	return qiniuConfig
}
