module gitee.com/liuyongchina/go-library

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/cactus/go-statsd-client v3.2.1+incompatible // indirect
	github.com/elastic/go-elasticsearch/v7 v7.12.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/onsi/gomega v1.12.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/qiniu/go-sdk/v7 v7.9.5
	github.com/spf13/cast v1.3.1 // indirect
	github.com/tidwall/gjson v1.7.5
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20210503195802-e9a32991a82e
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	gorm.io/driver/mysql v1.0.6
	gorm.io/gorm v1.21.9
)
